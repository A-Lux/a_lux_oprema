Vue.component('app-some', {
    props: ['img', 'name', 'price', 'count', 'caritemsall', 'productid'],
    template:`
            <div> 
                <img :src="img" style="width:10%;float:left;"/> 
                <div style="float:left;margin-left:5%;margin-top: 30px;width:25%">{{name}}</div>
                <div style="float:left;margin-left:5%;margin-top: 30px;width:15%">{{price}} тг.</div>
                <div style="float:left;margin-left:5%;margin-top: 30px;width:15%">{{price * count}} тг.</div>
                <div class="counter" style="float:right;width:8%;margin-top: 20px;">
                    <span class="minus" @click="$emit('minus')">
                        <i class="icon-minus" aria-hidden="true"></i>
                    </span>
                    <span class="counter_number">{{count}}</span>
                    <span class="plus"  @click="$emit('plus')">
                        <i class="icon-plus" aria-hidden="true"></i>
                    </span>
                </div>
                <div style="clear:both;"></div>
                <hr/>
            </div>  
        `,
    methods:{

    },
});

var car = Vue.component('app-car', {
    props: ['count', 'summ'],
    template:`
        <div>
            <div style="float:left;width:65%;">&nbsp;</div>
            <div style="float:left;"> 
                {{summ}} тг.
            </div>   
            <div style="float:right;margin-right:20px;"> 
                {{count}} товаров.
            </div>  
        </div>        
        `,
    methods:{

    },
});

var sample = new Vue({
        el: ".products",
        data: {
            info: data,
        },
        methods:{
            onMinus(key){
                if(this.info[key].count!=1){
                    axios.get('/updateCart?minus=1&productId=' + this.info[key].productid)
                        .then(response => (car.parentCount = response.data.count, car.allSumm = response.data.summ, this.info[key].count--));
                }
            },
            onPlus(key){
                axios.get('/updateCart?plus=1&productId=' + this.info[key].productid)
                    .then(response => (car.parentCount = response.data.count, car.allSumm = response.data.summ, this.info[key].count++));
            },
        },
        computed:{
            summ(){
                return this.price * this.count;
            }
        },
});

var car = new Vue({
    el: ".car",
    data: {
        parentCount: parentCountjs,
        allSumm: allSummjs,
    },
    created(){
        // let count = 0;
        // for (let i = 0; i < sample.info.length; i++) {
        //     count = count + parseInt(sample.info[0].count);
        // }
        // this.parentCount = count;
    }
});
