$(document).ready(function(){

	// aos
	AOS.init();
	// aos end

	// wow
	new WOW().init();
	// wow end

	// banner
	var owl = $('.main-banner__owl');
	owl.owlCarousel({
		items:1,
		loop: true,
		autoplay: true,
		autoplayTimeout: 15000,
		smartSpeed:800
	});
	// banner end

	$('.card-carousel').owlCarousel({
		dots: true,
		dotsContainer: '#custom-dots',
		items:1,
		loop: true,
		autoplay: true,
		autoplayTimeout: 2000,
		smartSpeed:800,
		margin: 3
	});
	$('.owl-dot').click(function () {
		owl.trigger('to.owl.carousel', [$(this).index(), 300]);
	});

	// superfish
	$('.open-menu').superfish({
		delay: 200,
		animation: {opacity:'show',height:'show'},
		animationOut: {opacity:'hide', height: 'hide'}
	});

	$('.phone').mask("8(999)-999-99-99");

	// search
	$('.search').click(function(){

		$(this).addClass('active-search')
		$('.header-contact-callback').css("display","none");
		$('.header-wrapper-left-link ul').addClass('active');
		$('.header-wrapper-left-contact').css({"margin-top":"2rem","margin-left":"1rem"});
		$('.header-contact-search form button img').addClass('active-img');
		$('.header-contact-search button').css("display","block");
	});
	$('.btn-close').click(function(e){
		e.preventDefault();
		$('.search').removeClass('active-search');
		$('.header-contact-callback').css("display","flex");
		$('.header-wrapper-left-link ul').removeClass('active');
		$('.header-wrapper-left-contact').css({"margin-top":"0","margin-left":"0"});
		$('.header-contact-search form button img').removeClass('active-img');
		$('.btn-close').css("display","none");
	});

	$('.contact-info ul li a').click(function(e){
		e.preventDefault();
	});

	// $('.left-bar a').click(function(e){
	// 	e.preventDefault();
	// 	var attr = $(this).attr('href');
	// 	if($(attr).css('display') == 'none'){
	// 		$(attr).animate({height: 'show'}, 500); 
	// 	}else{
	// 		$(attr).animate({height: 'hide'}, 500); 
	// 	}

	// });
    $(window).scroll(function(){
        if ($(window).scrollTop() >= 50) {
            $('.header-wrapper-right-link').addClass('fixed-left');
            $('.header-wrapper-left-link').addClass('fixed-right');

        }
        else {
            $('.header-wrapper-right-link').removeClass('fixed-left');
            $('.header-wrapper-left-link').removeClass('fixed-right');
        }
    });
    $(window).scroll(function(){
        if ($(window).scrollTop() >= 50) {
            $('.header-logo-media').addClass('fixed');
            $('.header-logo ').addClass('logo-img');

            $('.header-logo ').css("border-radius", 0);
            $('.header-logo ').css("box-shadow", "0 4px 2px -2px #999");
             }
        else {
            $('.header-logo-media').removeClass('fixed');
            $('.header-logo ').removeClass('logo-img');
            $('.header-logo ').css("border-radius", "0 0 1rem 1rem");
            $('.header-logo ').css("box-shadow", " 0px 0px 15px #999");
        }
    });


    $(".hamburger").on("click", function() {
		$(this).parent(".hamburger-wrapper").toggleClass("hamburger-active")
	});

	$('.cabinet-tab a').click(function(e){
		e.preventDefault();
		let attr = $(this).attr('href');

		$('.cabinet-tab a').removeClass('cabinet-active-tab');
		$(this).addClass('cabinet-active-tab');

		$('.wrapper-info').removeClass('wrapper-cabinet-active');
		$(attr).addClass('wrapper-cabinet-active');
	});

	$('.sea-password-btn').click(function(e){
		e.preventDefault();
		let attr = $(this).attr('href');
		let namebtn = $(this).html();
		if (namebtn == '<i class="fas fa-eye-slash"></i> Скрыть пароль') {
			var type = $(attr).attr('type','password');
			$(this).html('<i class="fas fa-eye"> Показать пароль');
		}else{
			var type = $(attr).attr('type','text');
			$(this).html('<i class="fas fa-eye-slash"></i> Скрыть пароль');
		}

	});
	$('.addAdress').click(function(e){
		e.preventDefault();
		let attr = $(this).attr('href');

		$(attr).append('<input type="text" class="form-control" name="address_two" placeholder="Ваш адрес">');
	});
	;
	// partners carousel

	$('.partners-carousel').owlCarousel({
		items:6,
		loop: true,
		autoplay: true,
		autoplayTimeout: 1500,
		smartSpeed:800,
		margin: 3,
		responsive : {
				0 : {
					items: 1
				},

				600 : {
					items: 3
				},

				1024 : {
					items: 6
				}
			}
	});


});

function down(id) {
	if(count > 1){
		$.get('http://localhost/a_lux_oprema/public/index.php/update?minus=1&productId='+id);
	}
	var $input = $(this).parent().find('input');
	var count = parseInt($input.val()) - 1;
	count = count < 1 ? 1 : count;
	$input.val(count);
	$input.change();

	return false;
};
function up(id) {
	$.get('http://localhost/a_lux_oprema/public/index.php/update?plus=1&productId='+id, function( data ) {
	  var $input = $(this).parent().find('input');
		$input.val(parseInt($input.val()) + 1);
		$input.change();
		return false;
	});
};

if (!Array.prototype.includes) {
  Object.defineProperty(Array.prototype, 'includes', {
    value: function (searchElement, fromIndex) {

      // 1. Let O be ? ToObject(this value).
      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }

      var o = Object(this);

      // 2. Let len be ? ToLength(? Get(O, "length")).
      var len = o.length >>> 0;

      // 3. If len is 0, return false.
      if (len === 0) {
        return false;
      }

      // 4. Let n be ? ToInteger(fromIndex).
      //    (If fromIndex is undefined, this step produces the value 0.)
      var n = fromIndex | 0;

      // 5. If n ≥ 0, then
      //  a. Let k be n.
      // 6. Else n < 0,
      //  a. Let k be len + n.
      //  b. If k < 0, let k be 0.
      var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

      function sameValueZero(x, y) {
        return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
      }

      // 7. Repeat, while k < len
      while (k < len) {
        // a. Let elementK be the result of ? Get(O, ! ToString(k)).
        // b. If SameValueZero(searchElement, elementK) is true, return true.
        // c. Increase k by 1.
        if (sameValueZero(o[k], searchElement)) {
          return true;
        }
        k++;
      }

      // 8. Return false
      return false;
    }
  })
};
