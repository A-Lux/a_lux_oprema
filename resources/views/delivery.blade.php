@include('header')

  <!-- delivery -->
  <section class="delivery">
    <div class="container" style="background-color: #fff;">
      <div class="row">
        <p class="delivery-title">Доставка</p>
      </div>
      <div class="row delivery-main__header">
        <div class="col-lg-4 col-md-4 col-sm-12 delivery__wrapper-header wow fadeInLeft">
          <div class="delivery__image">
            <img src="{{ asset('images/1.png') }}">
          </div>
          <div class="delivery__text">
            <p>{{ $delivery[0]->title }}</p>
            <p class="text__delivery">{{ $delivery[0]->text }}</p>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 delivery__wrapper-header wow fadeInUp">
          <div class="delivery__image">
            <img src="{{ asset('images/2.png') }}">
          </div>
          <div class="delivery__text">
            <p>{{ $delivery[1]->title }}</p>
            <p class="text__delivery">{{ $delivery[1]->text }}</p>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 delivery__wrapper-header wow fadeInRight">
          <div class="delivery__image">
            <img src="{{ asset('images/3.png') }}">
          </div>
          <div class="delivery__text">
            <p>{{ $delivery[2]->title }}</p>
            <p class="text__delivery">{{ $delivery[2]->text }}</p>
          </div>
        </div>
      </div>
      <div class="row delivery-main__body">
        <div class="col-lg-5 col-md-7 col-sm-12" style="margin: 0 auto;">
          <p class="delivery-main__body-title">способы оплаты</p>
          <img src="{{ asset('images/cards.png') }}" alt="" class="wow fadeInUp">
        </div>
      </div>
    </div>
  </section>
  <!-- end delivery -->

@include('footer')