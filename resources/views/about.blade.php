@include('header')
<section class="about">
    <div class="container" style="background-color: #fff;">
      <div class="row">
        <p class="about-title">преимущества нашей компании</p>
      </div>
      <div class="row about-main__header">
        @foreach($advantages as $advantage)
        <div class="col-lg-4 col-md-4 col-sm-12 about__wrapper-header wow fadeInLeft">
          <div class="about__image">
            <img src="{{ asset('storage/'.$advantage->img) }}">
          </div>
          <div class="about__text">
            <p>{!! strip_tags($advantage->title) !!}</p>
            <p class="text__about">{{ strip_tags($advantage->text) }}</p>
          </div>
        </div>
        @endforeach
      </div>
      <div class="row about-main__body">
        <div class="col-lg-9 col-md-9 col-sm-12" style="margin: 0 auto;">
          <p class="about-main__body-title">"{{ strip_tags($about->title) }}</p>
          <p class="about-main__body-text wow fadeInUp">
            {!! strip_tags($about->text) !!}
          </p>
        </div>
      </div>
    </div>
  </section>
  @include('footer')