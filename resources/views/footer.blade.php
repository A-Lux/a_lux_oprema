<!-- footer -->
  <footer>
    <div class="top-footer wow fadeInLeft">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 text-center">
            <a data-toggle="modal" data-target="#callback" href="#">получить бесплатную консультацию</a>
            <p>Оставьте заявку и наш менеджер свяжется с вами</p>
          </div>
        </div>
      </div>
    </div>
    <div class="medium-footer wow fadeInRight">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-12">
            <p class="footer-catalog-title">Каталог товаров</p>
            <ul class="navbar-nav catalog-tab-link">
              @foreach($header_cats as $cat)
                <li class="nav-item"><a href="{{ route('category' , $cat->id) }}" class="nav-link">{{ $cat->name }}</a></li>
              @endforeach
            </ul>
            <p class="footer-copy">&copy; Oprema Almaty.Все права защищены</p>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-12">
            <ul class="navbar-nav catalog-tab-link-middle">
              <li class="nav-item"><a href="{{ route('services') }}">Услуги</a></li>
              <li class="nav-item"><a href="{{ route('about') }}">О компании</a></li>
              <li class="nav-item"><a href="{{ route('partners') }}">Партнеры</a></li>
              <li class="nav-item"><a href="{{ route('contacts') }}">Контакты</a></li>
              <li class="nav-item"><a href="{{ route('delivery') }}">Доставка</a></li>
            </ul>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 footer-contact-right text-right">
            <div class="footer-contact">
              <p>Адреса и контакты:</p>
                Адрес: {!! setting('site.contacts') !!},<br>
                Тел:<a href="tel:{{ setting('site.phone_one') }}">{{ setting('site.phone_two') }}</a>, <a href="tel:{{ setting('site.phone_two') }}">{{ setting('site.phone_two') }}</a>,<br>
                E-mail: info@oprema.kz, sales@oprema.kz<br>
              <div id="wrapper-9cd199b9cc5410cd3b1ad21cab2e54d3" class="map">
                <div id="map-9cd199b9cc5410cd3b1ad21cab2e54d3" style="padding-top: 1rem;"></div><script>(function () {
                  var setting = {"height":210,"width":580,"zoom":18,"queryString":"улица Макатаева 117, Алматы, Казахстан","place_id":"ChIJs2U7Jq9ugzgRkA6ELP2Gefs","satellite":false,"centerCoord":[43.262915260155694,76.9275541313523],"cid":"0xfb7986fd2c840e90","id":"map-9cd199b9cc5410cd3b1ad21cab2e54d3","embed_id":"46717"};
                  var d = document;
                  var s = d.createElement('script');
                  s.src = 'https://1map.com/js/script-for-user.js?embed_id=46717';
                  s.async = true;
                  s.onload = function (e) {
                    window.OneMap.initMap(setting)
                  };
                  var to = d.getElementsByTagName('script')[0];
                  to.parentNode.insertBefore(s, to);
                })();</script>
                <a href="https://1map.com/map-embed?embed_id=46717"></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- end footer -->

  <!-- JS 
  ================================================== -->
  <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('js/main.js') }}"></script>
  <script src="{{ asset('js/aos.js') }}"></script>
  <script src="{{ asset('js/superfish.min.js') }}"></script>
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('js/wow.min.js') }}"></script>
  <script src="{{ asset('js/jquery.maskedinput.min.js') }}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  <script>
   @if(session('invalidLogin'))
    $(document).ready(function(){
      Swal.fire({
          title: "Неправильный логин или пароль",
          animation: true,
          customClass: {
            popup: 'custom-swal-popup'
          }
        })
    })   
    @endif 
    @if(session('invalidData'))
    $(document).ready(function(){
      toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "50000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }
      @if(strpos(session('invalidData')['errors'], "The password confirmation does not match.<br/>") !== false)
        toastr["error"]("Пароли не совпадают!")
      @endif
      @if(strpos(session('invalidData')['errors'], "Поле email должен быть действительным.<br/>") !== false)
        toastr["error"]("Некорректный email. Если вы уверены что ваш адрес электронной почты корректный, пожалуйста свяжитесь с нами.")
      @endif
      @if(strpos(session('invalidData')['errors'], "The email has already been taken.<br/>") !== false)
        toastr["error"]("Почта уже занята.")
      @endif
      @if(strpos(session('invalidData')['errors'], "The password must be at least 6 characters.<br/>") !== false)
        toastr["error"]("Пароль должен состоять более чем из 5 символов.")
      @endif
      {{-- @dd(session('invalidData')) --}}
    })
    @endif
    </script>
</body>
</html>
