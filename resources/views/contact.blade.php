@include('header')
<section class="contact">
  <div class="container" style="background-color: #fff;">
    <div class="row">
      <p class="contact-title">Контакты</p>
    </div>
    <div class="row">
      <div class="col-lg-6 col-md-12 col-sm-12 contact-info">
        <ul class="navbar-nav">
          <li class="nav-item">ТОО «OPREMA-Almaty» (ОПРЕМА-Алматы)</li>
          <li class="nav-item">{{ strip_tags(setting('site.contacts')) }}</li>
          <li class="nav-item">Тел:<a href="tel:{{ setting('site.phone_one') }}">{{ setting('site.phone_one') }}</a>, <a href="tel:{{ setting('site.phone_two') }}">{{ setting('site.phone_two') }}</a></li>
          <li class="nav-item">e-mail: <a href="mailto:info@oprema.kz">info@oprema.kz</a>, <a href="mailto:sales@oprema.kz">sales@oprema.kz</a></li>
        </ul>
      </div>

        <div class="col-sm-6 contact-info">
            <div class="map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2905.4424706531076!2d76.92493551499226!3d43.26310528558648!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38836eaf263b65b3%3A0xfb7986fd2c840e90!2z0YPQuy4g0JzQsNC60LDRgtCw0LXQstCwIDExNywg0JDQu9C80LDRgtGLIDA1MDAwMA!5e0!3m2!1sru!2skz!4v1562132629901!5m2!1sru!2skz" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
  </div>
</section>
@include('footer')
