@include('header')
<section class="services">
    <div class="container">
      <div class="row">
        <p class="services-title">Услуги</p>
        <p class="services-text wow fadeInUp">«OPREMA ALMATY» в настоящее время является одной из ведущих компаний в продаже оборудования для розлива пива, а также сырья и реактивов: солод, дрожжи, хмель, концентраты и т.д. для производства пива, кваса и безалкогольных напитков от ведущих мировых производителей на рынках Казахстана, Узбекистана, Киргизии. Также предоставляет весь спектр услуг по доставке, монтажу,  установке и каллибровки пиворазливочного оборудования, имеется аренда и  сервис по ремонту и диагностике оборудования.</p>
      </div>
      <div class="row services-main__header">
        @foreach($services as $service)
        <div class="col-lg-4 col-md-4 col-sm-12 services__wrapper-header wow fadeInLeft text-center">
          <div class="services__image">
            <img src="{{ asset('storage/'.$service->img) }}">
          </div>
          <div class="services__text">
            <p>{{ $service->title }}</p>
            <p class="text__services">{{ $service->text }}</p>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </section>
@include('footer')