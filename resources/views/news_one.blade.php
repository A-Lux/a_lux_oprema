@include('header')

    <section class="news-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="news-page-img">
                        <img src="{{ asset('storage/'.$news->img) }}">
                    </div>
                    <p class="news-page_title">
                        {{ $news->title }}
                    </p>
                    <p>
                            {!! $news->text !!}
                    </p>
                </div>
            </div>
        </div>
    </section>

@include('footer')