@include('header')

 <section class="partners">
        <p class="partners-title">Партнеры</p>
        <div class="container">
            <div class="row">
                <!-- partners card -->
                @foreach($partners as $partner)
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="partners-card">
                        <img src="{{ asset('storage/'.$partner->img) }}">
                    </div>
                </div>
                @endforeach
                <!-- end partners card -->
            </div>
        </div>
    </section>

@include('footer')