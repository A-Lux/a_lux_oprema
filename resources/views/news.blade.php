@include('header')

<section class="news">
    <div class="container" style="background-color: #fff;">
      <div class="row">
        <p class="news-title">новости</p>
      </div>
      <div class="row main-body-news">
        @foreach($news as $n)
          <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="card">
              <img src="{{ asset('storage/'.$n->img) }}">
              <div class="card-body">
                <p class="card-title">{{ $n->title }}</p>
                <small class="card-text text-muted">{{ date("Y-m-d", strtotime($n->created_at)) }}</small>
                <!-- <p class="card-text">{!! $n->short_description !!}</p>
 -->                <br>
                <a href="{{ route('news_one', $n->id) }}" class="btn">Читать далее</a>
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </section>

@include('footer')