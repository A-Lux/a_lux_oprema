<!DOCTYPE html>
<html lang="ru">
<head>
  <!--- Basic Page Needs
  ================================================== -->
  <meta charset="utf-8">
	<title>{{ $title }}</title>
	<meta name="description" content="">
	<meta name="author" content="">
  <!-- Mobile Specific Metas
   ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

  <!-- CSS
  ================================================== -->
  <link rel="stylesheet" href="{{ asset('css/main.css') }}">
  <!-- CSS Plugins
  ================================================== -->
  <link rel="stylesheet" type="text/css" href="{{ asset('css/animate.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/aos.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/owl.carousel.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/owl.theme.default.min.css') }}">
  <!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="#">
  <!-- Font Link
  ================================================== -->
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&display=swap" rel="stylesheet">
  <!-- CSS FontAwesome
  ================================================== -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
  
  <script type="text/javascript">
    var favIDs= [];

    function addToCart(id) {
      if(favIDs.includes(id)){
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000
        });

        Toast.fire({
          type: 'success',
          title: 'Товар уже находится в корзине!'
        })
      }
      else{
        $.get("{{ route('add') }}?productId="+id);
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000
        });

        Toast.fire({
          type: 'success',
          title: 'Товар в Корзине!'
        })
        document.getElementById('cartcount').innerHTML = parseInt(document.getElementById('cartcount').innerHTML)+1;
        document.getElementById('cartcount-mobile').innerHTML = parseInt(document.getElementById('cartcount-mobile').innerHTML)+1;
        favIDs.push(id)
      }
   
    }
    
    function callback() {
      $.ajaxSetup({
            headers:
                { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
            });
            if(document.getElementById('CallbackName').value.length>0 && document.getElementById('CallbackPhone').value.length>0){
              $.post("{{ route('request') }}", 
                { 
                  name: document.getElementById('CallbackName').value,
                  phone: document.getElementById('CallbackPhone').value,
               }, 
               function( data ) {
                  if(data == "1"){
                      Swal.fire(
                          'Ваша заявка принята!',
                          'Наш менеджер скоро свяжется с вами',
                          'Принято'
                      )                    
                  }
                  else{
                      if(data == "2"){
                          $('#signin').modal('show');
                      }
                      else{
                          Swal.fire(
                            'Что то пошло не так',
                            'Произошла какая то проблема',
                            'Закрыть'
                          )
                      }
                  }
              });
            }
            else{
              Swal.fire(
                'Ошибка!',
                'Пожалуйста, заполните все поля',
                'Хорошо'
              )
            }
    }
  </script>
</head>
<body>

  <!-- header -->
  <header class="desktop-header wow fadeInDown " style="width: 100%; background-color: #fff;">
    <div class="container-fluid top-header">
      <div class="row">
        <div class="container">
          <ul class="navbar-nav">
            @if(\Auth::check())
              <li class="nav-item"><a href="{{ route('cabinet') }}" class="nav-link">{{ \Auth::user()->name }} / </a></li>
              <li class="nav-item"><a href="{{ route('logout') }}" class="nav-link">&nbsp;Выйти</a></li>
            @else
              <li class="nav-item"><a href="#" data-toggle="modal" data-target="#login" class="nav-link">Войти / </a></li>
              <li class="nav-item"><a href="#" data-toggle="modal" data-target="#signin" class="nav-link">&nbsp;Регистрация</a></li>
            @endif
          </ul>
        </div>
      </div>
    </div>
    <div class="container medium-header">
      <div class="row"> 
        <div class="col-lg-4">
          <div class="header-wrapper-right">
            <div class="header-wrapper-right-contact">
              <div class="row header-contact">
                <div class="col-lg-8 header-contact-location">
                  <img src="{{ asset('images/icon-location.png') }}" alt="">
                  <div class="wrapper-location">
                    <small class="text-muted">Адрес и контакты</small>
                    <br>
                    <p>{!! setting('site.contacts') !!}</p>
                  </div>
                </div>
                <div class="col-lg-4 header-contact-phone">
                  <a href="tel:{{ setting('site.phone_one') }}">{{ setting('site.phone_one') }}</a><br>
                  <a href="tel:{{ setting('site.phone_two') }}">{{ setting('site.phone_two') }}</a><br>
                </div>
              </div>
            </div>
            <div class="header-wrapper-right-link ">
              <ul class="navbar-nav open-menu">
                <li class="nav-item active-item"><a href="{{ route('category') }}" class="nav-link active">каталог товаров</a>
                  <!-- open menu -->
                  <ul class="navbar-nav open-menu__block">
                    @foreach($header_cats as $cat)
                      <li class="nav-item"><a href="{{ route('category', $cat->id) }}" class="nav-link">{{ $cat->title }}</a></li>
                    @endforeach
                  </ul>
                </li>
                <li class="nav-item"><a href="{{ route('services') }}" class="nav-link">услуги</a></li>
                <li class="nav-item"><a href="{{ route('about') }} " class="nav-link">о компании</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-12 col-sm-12 header-logo-media">
          <div class="header-logo">
            <a href="{{ route('index') }}">
              <img src="{{ asset('images/logo.jpg') }}" alt="лого">
             </a>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="header-wrapper-left">
            <div class="header-wrapper-left-contact">
              <div class="row header-contact">
                <div class="col-lg-7 header-contact-callback">
                  <img src="{{ asset('images/icon-phone.png') }}" alt="">
                  <div class="wrapper-callback">
                    <a data-toggle="modal" data-target="#callback" href="#">Перезвоните мне</a>
                    <br>
                    <small class="text-muted">Бесплатная консультация</small>
                  </div>
                </div>
                <div class="col-lg-5 header-contact-search">
                  <form action="{{ route('search') }}", method="GET">
                    <input class="form-control search" name="search" type="search" placeholder="Поиск">
                    <button type="submit"><img src="{{ asset('images/ico-search.jpg') }}" alt=""></button>
                    <button type="submit" class="btn btn-close" style="display: none;">Закрыть</button>
                  </form>
                  
                  @if(Session::get('cart') !== null)
                  @php 
                    $count = 0;
                    foreach(Session::get('cart') as $k => $v){
                        $count += $v[0];
                    }
                  @endphp
                  @endif
                  <a href="{{ route('cart') }}"><img width="20px" src="{{ asset('images/shopping-cart.png') }}"> Корзина <span id="cartcount">@if(Session::get('cart') !== null){{ $count }} @else 0 @endif</span></a>
                </div>
              </div>
            </div>
            <div class="header-wrapper-left-link">
              <ul class="navbar-nav">
                <li class="nav-item"><a href="{{ route('news') }}" class="nav-link">новости</a></li>
                <li class="nav-item"><a href="{{ route('contacts') }}" class="nav-link">контакты</a></li>
                <li class="nav-item"><a href="{{ route('delivery') }}" class="nav-link">доставка</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <!-- end header -->

  <!-- mobal menu burger -->
  <header class="mobile__menu header ">
        <div class="container">
            <div class="menu-icon">
                <content class="mob-view">
                    <input id="hamburger" class="hamburger" type="checkbox">
                    <label class="hamburger" for="hamburger">
                        <i></i>
                        <text style="display:none;">
                            <close>закрыть</close>
                            <open>меню</open>
                        </text>
                    </label>
                    <section class="drawer-list mob-num text-center">
                      <div class="col-lg-4 header-contact-phone">
                        <a href="tel:{{ setting('site.phone_one') }}">{{ setting('site.phone_one') }}</a><br>
                        <a href="tel:{{ setting('site.phone_two') }}">{{ setting('site.phone_two') }}</a><br>
                      </div>
                        <ul class="open-menu-mobile">
                            <li>
                                <a href="{{ route('category') }}">Каталог товаров</a>
                            </li>
                            <li>
                                <a href="{{ route('services') }}">Услуги</a>
                            </li>
                            <li>
                                <a href="{{ route('about') }}">О компании</a>
                            </li>
                            <li>
                                <a href="{{ route('news') }}">Новости</a>
                            </li>
                            <li>
                                <a href="{{ route('contacts') }}">Контакты</a>
                            </li>
                            <li>
                                <a href="{{ route('delivery') }}">Доставка</a>
                            </li>
                            <li>
                              <a href="{{ route('cart') }}"><img width="20px" src="{{ asset('images/shopping-cart.png') }}"> Корзина <span id="cartcount-mobile">@if(Session::get('cart') !== null){{ $count }} @else 0 @endif</span></a>
                            </li>
                        </ul>
                    </section>
                </content>
            </div>
        </div>
    </header>
  <!-- end modal menu burger -->

  <!-- header modal login -->
  <div class="modal fade login" id="login">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Войти</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
            @csrf
            <div class="form-group">
              <label for="InputEmail">Email</label>
              <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" required autofocus name="email" id="InputEmailLogin" aria-describedby="emailHelp" placeholder="Введите email ...">
              @if ($errors->has('email'))
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
            </div>
            <div class="form-group">
              <label for="InputPassword">Пароль</label>
              <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required id="InputPasswordLogin" placeholder="Введите пароль ...">
              @if ($errors->has('password'))
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
            </div>
            <div class="form-group form-check">
              <input type="checkbox" class="form-check-input" id="Check"  name="remember" {{ old('remember') ? 'checked' : '' }}>
              <label class="form-check-label" for="Check">Запомнить меня</label>
            </div>
            <button type="submit" class="btn">Войти</button>
          </form>
        </div>
        <div class="modal-footer">
          <a class="nav-link" href="/password/reset">Забыли пароль ?</a>
        </div>
      </div>
    </div>
  </div>
  <!-- end header modal login -->

  <!-- header modal sign in -->
  <div class="modal fade signin" id="signin">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Авторизация</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
            @csrf
            <div class="form-group">
              <label for="InputEmail">Email</label>
              <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" autofocus required id="InputEmail" aria-describedby="emailHelp" placeholder="Введите email ...">
              @if ($errors->has('email'))
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
            </div>
            <div class="form-group">
              <label for="InputName">Имя</label>
              <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required id="InputName" placeholder="Введите имя ...">
              @if ($errors->has('name'))
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
              @endif
            </div>
            <div class="form-group">
              <label for="InputPassword">Пароль</label>
              <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required id="InputPassword" placeholder="Введите пароль ...">
              @if ($errors->has('password'))
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
            </div>
            <div class="form-group">
              <label for="InputSurename">Подтвердите пароль</label>
              <input type="password" class="form-control" id="inputPasswordCheck" type="password" class="form-control" name="password_confirmation" required placeholder="Подтвердите пароль ...">
            </div>
            <button type="submit" class="btn">Регистрация</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- end header modal sign in -->

  <!-- header modal callback -->
  <div class="modal fade callback" id="callback">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Получить консультацию</h5>
          <button type="button" class="close close-feedback" data-dismiss="modal" aria-label="Close" >
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="form-group">
              <label for="CallbackName">Имя</label>
              <input name="name" required type="text" class="form-control" id="CallbackName"  placeholder="Введите имя ...">
            </div>
            <div class="form-group">
              <label for="CallbackPhone">Телефон</label>
              <input name="phone" required type="text" class="form-control phone" id="CallbackPhone">
            </div>
            <button onclick="callback()" class="btn">Отправить</button>
        </div>
      </div>
    </div>
  </div>
  <!-- end header modal callback -->
