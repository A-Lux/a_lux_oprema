@include('header')
  <script>
  window.onload = function () { 
    if(window.location.href.indexOf('#order') != -1){
      $('.wrapper-card-user').toggleClass('wrapper-cabinet-active');
      $('.wrapper-card-order').toggleClass('wrapper-cabinet-active');
      $($('.nav-link')[19]).toggleClass('cabinet-active-tab');
      $($('.nav-link')[20]).toggleClass('cabinet-active-tab');
    }
  }
  </script>
  <section class="cabinet">
    <div class="container">
      <div class="row">
        <div class="col-lg-3">
          <ul class="navbar-nav cabinet-tab wow fadeInLeft">
            <li class="nav-item"><a href="#info" class="nav-link cabinet-active-tab">Мои данные</a></li>
            <li class="nav-item"><a href="#order" class="nav-link">История заказов</a></li>
          </ul>
        </div>
        <div class="col-lg-8">
          <div class="wrapper-card-user wrapper-info wrapper-cabinet-active wow fadeInRight" id="info">
            <p class="wrapper-card-user-title">Мои данные</p>
            <form action="{{ route('edit') }}" method="POST">
              @csrf
              <div class="row form-cabinet">
                <div class="col-lg-6"> 
                  <span class="left-card-user">Ваш логин:</span> 
                </div>
                <div class="col-lg-6">
                  <p class="right-card-user">{{ \Auth::user()->email }}</p>
                </div>
              </div> 
              <div class="row form-cabinet">
                <div class="col-lg-6">
                  <span class="left-card-user">Сменить пароль:</span>
                </div>
                <div class="col-lg-6">
                  <input type="password" class="form-control" id="sea-password" name="" value="">
                  <a class="sea-password-btn" href="#sea-password"><i class="fas fa-eye"></i> Показать пароль</a>
                </div>
              </div>
              <div class="row form-cabinet">
                <div class="col-lg-6">
                  <span class="left-card-user">Ваше имя:</span>
                </div>
                <div class="col-lg-6">
                  <input type="text" class="form-control" name="name" value="{{ \Auth::user()->name }}" placeholder="admin"> 
                </div>
              </div>
              <div class="row form-cabinet">
                <div class="col-lg-6">
                  <span class="left-card-user">Контактный телефон:</span>
                </div>
                <div class="col-lg-6">
                  <input type="text" class="form-control" value="{{ \Auth::user()->phone }}" name="phone"> 
                </div>
              </div>
              <div class="row form-cabinet">
                <div class="col-lg-6">
                  <span class="left-card-user">Адрес доставки:</span>
                </div>
                <div class="col-lg-6">
                  <input type="text" name="address_one" class="form-control" name="address_one" value="{{ \Auth::user()->address_one }}" placeholder="Ваш адрес">
                  <div id="addAdress"></div>
                </div>
              </div>
              <div class="row form-cabinet">
                <div class="col-lg-6">
                  <span class="left-card-user">Индекс:</span>
                </div>
                <div class="col-lg-6">
                  <input type="text" name="index" class="form-control" name="index" value="{{ \Auth::user()->client_index }}" placeholder="Ваш почтовый индекс">
                </div>
              </div>
              <div class="row form-cabinet">
                <div class="col-lg-12">
                  <button class="btn save-btn" type="submit">Сохранить</button>
                </div>
              </div>
            </form>
          </div>
          <div class="wrapper-card-order wrapper-info" id="order">
            <p class="wrapper-card-order-title">История заказов</p>
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">Номер заказа</th>
                  <th scope="col">Дата</th>
                  <th scope="col">Адрес</th>
                  <th scope="col">Сумма заказа</th>
                </tr>
              </thead>
              <tbody>
                @foreach($orders as $order)
                  <tr>
                    <th scope="row">#{{$order->id}}</th>
                    <td>{{$order->created_at}}</td>
                    <td>{{$order->address}}</td>
                    <td>{{$order->total_sum}}тг</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>


@include('footer')