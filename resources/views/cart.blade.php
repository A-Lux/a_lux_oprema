@include('header')

<section class="basket">
    <script type="text/javascript">
        var products = [];
        @foreach($products as $product)
            products.push({
                name: "{{ $product->name }}",
                price: {{ $product->price }},
                amount: {{ $product->amount }},
                id: {{ $product->id }}
            });
        @endforeach
        let token = document.head.querySelector('meta[name="csrf-token"]');
        var totalSum = 0;
        
        function refreshTotalSum(){
            totalSum = 0;
            for (i=0; i<products.length; i++){
                totalSum=totalSum+products[i].price*products[i].amount;
            }
            document.getElementById('FinalResult').innerHTML='Итого:'+ totalSum +' тг'; 
        }        

        $(document).ready(function (){
            refreshTotalSum();
        });

        function deleteProduct(id) {   
            document.cookie = 'product'+id+'=; Max-Age=-99999999;';
            var index = getIndexByValue(products, id);
            products[index].amount = 0;
            refreshTotalSum();
            $.get( "{{ route('remove') }}?productId="+id, function( data ) {});
            document.getElementById('cartcount-mobile').innerHTML = parseInt(document.getElementById('cartcount').innerHTML)-1;
            document.getElementById('cartcount').innerHTML = parseInt(document.getElementById('cartcount').innerHTML)-1;
            $('#productString'+id).remove()
        }
        function getIndexByValue(arr, value) {
          for (var i=0, iLen=arr.length; i<iLen; i++) {
            if (arr[i].id == value) return i;
          }
        }
        function plusAmount(id){
            var obj = document.getElementById('inputAmount'+id)
            obj.stepUp()
            var index = getIndexByValue(products, id);
            document.getElementById('result'+id).innerHTML = products[index].price*obj.value + 'тг';
            products[index].amount = products[index].amount + 1;
            refreshTotalSum();
            $.get( "{{ route('update') }}?plus=1&productId="+id, function( data ) {});
        }
        
        function minusAmount(id){
            var obj = document.getElementById('inputAmount'+id)
            if(obj.value>1){
                obj.stepDown()
                var index = getIndexByValue(products, id);
                document.getElementById('result'+id).innerHTML = products[index].price*obj.value + 'тг';
                products[index].amount = products[index].amount - 1;
                refreshTotalSum();
                $.get( "{{ route('update') }}?minus=1&productId="+id, function( data ) {});
            }
        }

        function sendOrder(){
            $.ajaxSetup({
            headers:
                { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
            });
            $.post("{{ route('order') }}", { "products": JSON.stringify(products) }).done(function( data ) {
                if(data == "1"){
                    Swal.fire(
                      'Ваш заказ принят!',
                      'Наш менеджер скоро свяжется с вами',
                      'Принято'
                    ).then((result) => {
                      if (result.value) {
                        window.location.replace('http://opremaf.ibeacon.kz/cabinet#order');
                      }
                    })  
                }
                else{
                    if(data == "2"){
                        $('#login').modal('show');
                    }
                    else{
                        Swal.fire(
                          'Что то пошло не так',
                          'Произошла какая то проблема',
                          'Закрыть'
                        )
                    }
                }
            }); 
        }


    </script>

        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <p class="basket-title">Корзина <i class="fas fa-shopping-basket"></i></p>
                    @php $totalSum = 0; @endphp
                    @if(count($products)>0)
                    @foreach($products as $product)
                    <div class="box-cart" id="productString{{$product->id}}">
                        <div class="product-kainar">
                            <!-- card -->
                            
                                <div class="cart-body">
                                    <img class="product" src="{{ asset('storage/'.$product->img) }}">
                                    <p>{{ $product->name }}</p>
                                    <div class="quantity">
                                        <p>Количество</p>
                                        <div class="input">
                                            <input type="number" class="form-control" value="{{ $product->amount }}" id="inputAmount{{$product->id}}">
                                            <span class="up" onclick="plusAmount({{$product->id}})"> <img src="{{ asset('images/up.jpg') }}"></span>
                                            <span class="down" onclick="minusAmount({{$product->id}})"><img src="{{ asset('images/down.jpg') }}"></span>
                                        </div>
                                    </div>
                                    <p class="price" id="result{{$product->id}}">{{ $product->price*$product->amount }} тг.</p>
                                    <a class="close" href="#" onclick="deleteProduct({{$product->id}})"><img src="{{ asset('images/close.jpg') }}"></a>
                                </div>
                            
                            <!-- end card -->
                        </div>
                    </div>
                    @endforeach
                    <div class="footer-cart">
                        <div class="total-price">
                            <p class="total" id="FinalResult"></p>
                            {{-- <p class="deliverybtn">Доставка: 14 600 руб.</p> --}}
                        </div>
                        <a class="buy btn" href="#" onclick="sendOrder()">Оформить заказ</a>
                    </div>
                    @else
                        <div class="product-kainar">
                            <!-- card -->
                            
                                <div class="cart-body">
                                    <div class="quantity">
                                        <p>Корзина пуста</p>
                                    </div>
                                </div>
                            
                            <!-- end card -->
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>

@include('footer')