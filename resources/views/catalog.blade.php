@include('header')

  <section class="instrumenti">
    <div class="container" style="background-color: #fff;">
      <div class="row">
        <div class="instrumenti-title">{{ $category->title }}</div>
      </div>
      <div class="row main-body-instrumenti">
        <div class="col-lg-4">
          <ul class="navbar-nav left-bar">
          	@foreach($categories as $category)
	            	<li class="nav-item">
                  <a href="{{ route('category', $category->id) }}" class="nav-link">
                    {{ $category->title }}
                  </a>
                </li>
	          @endforeach
          </ul>
        </div>
        <div class="col-lg-8">
          <div class="row">
            <div class="col-lg-12">
            	@php $parents = array_reverse($parents) @endphp
            	 <a href="{{ route('category', 0) }}">Каталог</a>
                 @foreach($parents as $parent) 
                  /<a href="{{ route('category', $parent->id) }}">{{ $parent->name }}</a>
                 @endforeach
           </div>
          </div>
          <div class="row">
            @foreach($goods as $good)
            <div class="col-lg-4">
              <a href="{{ route('product', $good->id) }}" class="nav-link title"><div class="card">
                <div class="card-price">
                  <p>{{ $good->price }}тг.</p>
                </div>
                <img src="{{ asset('storage/'.$good->img) }}" alt="">
              </a>
                <div class="card-body">
                  <div class="card-text"><h6>{{ strip_tags($good->title) }}</h6></div>
                  <span onclick="addToCart({{$good->id}})" class="btn">Заказать</span>
                  <a href="{{ route('product', $good->id) }}" class="nav-link title"><span class="btn">Подробнее</span></a>
                </div>
              </div>
              
            </div>
            @endforeach
            {{ $goods->links() }}
          </div>
        </div>
      </div>
    </div>
  </section>

@include('footer')
