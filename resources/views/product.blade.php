@include('header')

  <!-- more card info -->
  <section class="more-card">
    <div class="container" style="background-color: #fff;">
      <div class="row">
        <div class="col-lg-8 col-md-12 col-sm-12 more-card-header">
          <div class="row">
            <div class="col-lg-6 more-card-header-left">
              <p>{{ $product->name }} </p>
              <div class="card">
                <img src="{{ asset('storage/'.$product->img) }}">
              </div>
            </div>
            <div class="col-lg-6 more-card-header-right">
              <p>Преимущества {{ $product->name }}:</p>
              <div class="test">
                {!! $product->preimuzhestva !!}
              </div>
              <a onclick="addToCart({{$product->id}})" class="btn">Заказать</a>
            </div>
          </div>
        </div>
      </div>
      <div class="row more-card-body table-responsive">
        <div class="col-lg-8 col-md-12 col-sm-12">
          {!! $product->opisanie !!}
        </div>
      </div>
    </div>
  </section>
  <!-- end more card info -->

@include('footer')
