@include('header')

  <!-- main banner -->
  <section class="main-banner">
    <div class="main-banner__owl owl-carousel owl-theme">
      <!-- slide1 -->
      @foreach($slider as $slide)
      <div class="main-banner-slide1" style="background-image: {{ asset('storage/'.$slide->img) }}">
        <div class="container">
          <div class="row">
            <div class="col-lg-7 col-md-9 col-sm-10">
              <div class="banner-text">
                <a href="{{ route('category') }}">Перейти в каталог <i class="fas fa-level-up-alt"></i></a>
                <p class="text-center">{{ strip_tags($slide->title) }}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </section>
  <!-- end main banner -->

  <!-- advantages -->
  <section class="advantages">
    <div class="container">
      <div class="row">
        <p class="advantages-title">преимущества нашей компании</p>
      </div>
      <div class="row advantages-card">
        @foreach($advantages as $advantage)
        <div class="col-lg-3 col-md-3 col-sm-12 advantages__wrapper wow fadeInLeft">
          <div class="advantages__image">
            <img src="{{ asset('storage/'.$advantage->img) }}">
          </div>
          <div class="advantages__text text-center">
            <p class="text__advantages">{{ strip_tags($advantage->title) }}</p>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </section>
  <!-- end advanteges -->

  <!-- product card -->
  <section class="home-product-card">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="home-product-card__title">популярные товары</div>
          <div class="row popular-card-home wow fadeInLeft">
            <div class="col-lg-6 col-md-12 col-sm-12">
            <div class="row">
              @foreach($popularnie as $good)
                <div class="col-lg-6 col-md-6 col-sm-12">
                  <div class="card">
                    <div class="card-price">
                      <p>{{ $good->price }}тг.</p>
                    </div>
                    <img src="{{ asset('storage/'.$good->img) }}" alt="">
                    <div class="card-body">
                      <div class="card-text"><p>{{ $good->name }}</p>
                        <a onclick="addToCart({{$good->id}})" class="btn">Заказать</a>
                        <a href="{{ route('product', $good->id) }}" class="btn">Подробнее</a>
                      </div>
                    </div>
                  </div>
                </div>
              @endforeach
              </div>
            </div>
        <div class="col-lg-6 card-home-carousel wow fadeInRight">
          <!-- dots -->
          <ul class="owl-dots custom-dots text-center" id="custom-dots">
            <li class="owl-dot active" role="button"></li>
            <li class="owl-dot" role="button"></li>
            <li class="owl-dot" role="button"></li>
            <li class="owl-dot" role="button"></li>
          </ul>
          <!-- dots end -->
          <div class="card-carousel owl-carousel owl-theme">
            <!-- slide1 -->
            @foreach($discounts as $discount)
              <div class="card-carousel-wrapper">
                <div class="row">
                  <div class="col-sm-12">
                    <p class="card-carousel-title">{{ $discount->product->name }}</p>
                    <div class="card-carousel-price">
                      <p>{{ $discount->discount }} тг.</p>
                      <span>{{ $discount->product->price }} тг.</span>
                    </div>                    
                  </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 col-md-8">
                      <p class="card-carousel-text">
                        {{ strip_tags($discount->product->opisanie) }}
                      </p>
                    </div>
                    <div class="col-lg-4 col-md-4">
                      <img src="{{ asset('storage/'.$discount->product->img)}}">
                    </div>
                </div>
                <a href="{{ route('product', $discount->product->id )}}">Подробнее...</a>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- end product card -->

  <!-- video -->
  <section class="video-client">
    <div class="container">
      <div class="row">
        <p class="video-client-title">Видео-отзывы от наших клиентов</p>
      </div>
      <div class="row">
      @foreach($videootzivi as $video)
      <div class="col-lg-3 col-md-6"> 
        <div class="card">
          <iframe src="{{ $video->video }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          <div class="card-body">
            <p>{{ $video->name }}, {{ $video->komania }}, {{ $video->dolzhnost }}</p>
          </div>
        </div>
      </div>
      @endforeach 
      </div>
    </div>
  </section>
  <!-- end video -->

  <!-- news -->
  <section class="home-news">
    <div class="container">
      <div class="row">
        <div class="home-news-title">Новости</div>
      </div>
      <div class="row home-news-card">
        @foreach($news as $n)
        <div class="col-lg-4 col-md-6 col-sm-12 wow fadeInLeft">
          <div class="card">
            <img src="{{ asset('storage/'.$n->img) }}">
            <div class="card-body">
              <p class="card-title">{{ $n->title }}</p>
              <small class="card-text text-muted">{{ date('Y-m-d', strtotime($n->created_at)) }}</small>
              <p class="card-text">{{ $n->description }}</p>
              <a href="{{ route('news_one', $n->id) }}" class="btn">Читать далее</a>
            </div>
          </div>
        </div>
        @endforeach
      </div>
      <a class="btn more-btn" href="{{ route('news') }}">Все новости</a>
    </div>
  </section>
  <!-- end news -->

  <!-- partners -->
  <section class="partners">
    <div class="container">
      <div class="row">
        <p class="partners-title">Партнеры</p>
      </div>
      <div class="row">
        <div class="partners-carousel owl-carousel owl-theme">
          <!-- slide1 -->
          @foreach($partners as $partner)
          <div class="partners-card">
            <img src="{{ asset('storage/'.$partner->img) }}">
          </div>
         @endforeach
        </div>
      </div>
    </div>
  </section>
  <!-- end partners -->

  
@include('footer')
