<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен быть как минимум 6 символов длиной.',
    'reset' => 'Ваш пароль был изменён',
    'sent' => 'Мы отправили вам e-mail со ссылкой для восстановления пароля',
    'token' => 'данный токен не является валидным.',
    'user' => "Пользователь с указанным адресом почты не найден в нашей базе данных.",

];
