<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FilterCategory extends Model
{
    public function childs()
    {
        return $this->hasMany(FilterChild::class, 'filter_id');
    }
}
