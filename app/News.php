<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = [
        'title',
        'description',
        'keywords',
        'name',
        'text',
		'kr_opisanie',
        'url',
        'status',
        'sort',
        'created_at',
        'updated_at',
        'img',
    ];
}
