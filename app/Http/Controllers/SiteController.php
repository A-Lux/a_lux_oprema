<?php

namespace App\Http\Controllers;

use App\Dostavka;
use App\MenuSite;
use App\News;
use App\Partner;
use App\Preimuzhesva;
use App\Slider;
use App\Sovety;
use App\Tovary;
use App\Uslugy;
use App\About;
use App\DiscountsSlider;
use App\Videootzivi;
use Illuminate\Http\Request;
use App\Http\Filter\ProductFilter;

class SiteController extends FrontendController
{
    public function index()
    {
        $model = MenuSite::where(["url" => "/"])->first();
        $menu = $this->menu;
        $menufooter = $this->menufooter;
        $categoryes = $this->categoryes;
        $advantages = Preimuzhesva::limit(4)->orderBy('id')->get();
        $slider = Slider::get();
        $videootzivi = Videootzivi::get();
//        $soveties = Sovety::get();
        $news = News::limit(3)->orderBy('id', 'DESC')->get();
        $popularnie = Tovary::where(['popularnie' => 1])->limit(2)->get();
		$partners = Partner::get();
		$title = $model->title;
		$description = $model->description;
        $header_cats = $this->getCategoryes();
        //TODO
        $discounts = DiscountsSlider::get();
        foreach ($discounts as $discount) {
            $discount->product = Tovary::find($discount->product_id);
            $discount->product->opisanie = $this->cutStr($discount->product->opisanie, 550);
        }
        //$Slider with products
        // dd($discounts);
        return view('index', compact('model', 'menu', 'advantages', 'slider', 'videootzivi', 'news', 'categoryes', 'menufooter', 'popularnie', 'partners', 'title', 'description', 'header_cats', 'discounts'));
    }

    public function contacts()
    {
        $title = 'Контакты';
        $header_cats = $this->getCategoryes();
        return view('contact', compact('title', 'header_cats'));
    }

    public function delivery()
    {
        $title = 'Доставка';
        $delivery = Dostavka::get();
        $header_cats = $this->getCategoryes();
        return view('delivery', compact('delivery', 'title', 'header_cats'));
    }

    public function about()
    {
        $title = 'О нас';
        $header_cats = $this->getCategoryes();
        $advantages = Preimuzhesva::get()->slice(4);
        $about = About::first();
        return view('about', compact('title', 'header_cats', 'advantages', 'about'));
    }

    public function partners()
    {
        $title = 'Наши партнёры';
        $header_cats = $this->getCategoryes();
        $partners = Partner::get();
        return view('partners', compact('title', 'header_cats', 'partners'));
    }
    
    public function services(){
        $title = 'Услуги';
        $services = Uslugy::get();
        $header_cats = $this->getCategoryes();
        return view('services', compact('title', 'header_cats', 'services'));
    }
}
