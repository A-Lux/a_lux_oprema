<?php

namespace App\Http\Controllers;

use App\News;
use App\Send;
use App\Tovary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\CategoryTovary;

class SearchController extends FrontendController
{
    public function index()
    {
        $category = new \StdClass;
        $category->title = 'Поиск';
        $parents = [];
      
        $categories = CategoryTovary::where('parent_id', 0)->get();
        $arr = [];
        foreach ($categories as $v){
            $arr[] = $v->id;
            $temp_one = CategoryTovary::where('parent_id', $v->id)->get();
            if(isset($temp_one)){
                if(!$temp_one->isEmpty()){
                    foreach ($temp_one as $i){
                        $arr[] = $i->id;
                    }
                }
            }            
        }
        
        $goods = Tovary::where('name', 'like', '%' . Input::get('search') . '%')->paginate(6);
        $title = $category->title;
        $header_cats = $this->getCategoryes();
        // dd($parents);
        return view('catalog', compact('category', 'categories', 'goods', 'title', 'parents', 'header_cats' ));
    }
}
