<?php

namespace App\Http\Controllers;

use App\Order;
use App\Ordering;
use Illuminate\Http\Request;
use App\Tovary;
use Illuminate\Support\Facades\Session;

class CartController extends FrontendController
{
    public function addToCart()
    {
        $productId = $_GET['productId'];

//        Session::flush();
        if (Session::has("cart.$productId.0"))
            Session::increment("cart.$productId.0", 1);
        else{
            Session::push("cart.$productId", 1);

            $product = Tovary::find($productId);
            Session::push("cart.$productId", $product->price);
        }

//        foreach(Session::get('cart') as $k => $v){
//            echo $v;die;
//        }

        Session::push("cartcount", 1);
        return back();
    }

    public function order()
    {
//        dd(Session::all());
//        foreach(Session::get('cart') as $k => $v){
//            $product = Tovary::find($k);
//            dd($product->price);
//        }
        $menu = $this->menu;
        $menufooter = $this->menufooter;
        $categoryes = $this->categoryes;
		$title = 'Корзина';
		$description = 'Корзина';
        $products = [];
        if(!is_null(session('cart'))){
            foreach(session('cart') as $id => $v){
                $temp = Tovary::find($id);
                $temp->amount = $v[0];
                $products[]= $temp;
            }
        }
		$header_cats = $this->getCategoryes();
        return view('cart', compact('menu', 'categoryes', 'menufooter', 'title', 'description', 'header_cats', 'products'));
    }

    public function updateCart()
    {
        $productId = $_GET['productId'];

        if(isset($_GET['minus']))
            Session::increment("cart.$productId.0", -1);
        if(isset($_GET['plus']))
            Session::increment("cart.$productId.0", 1);

        Session::forget("cartcount");

        $count = 0;
        $summ  = 0;
        foreach(Session::get('cart') as $k => $v){
            $count += $v[0];
            $summ  += $v[0] * $v[1];
        }

        Session::push("cartcount", $count);

        $array = ['count' => $count, 'summ' => $summ];
        return json_encode($array);
    }

    public function delete()
    {
        $productId = $_GET['productId'];

        Session::forget("cart.$productId");

        Session::forget("cartcount");

        $count = 0;
        $summ  = 0;
        foreach(Session::get('cart') as $k => $v){
            $count += $v[0];
            $summ  += $v[0] * $v[1];
        }

        Session::push("cartcount", $count);

        $array = ['count' => $count, 'summ' => $summ];
        return json_encode($array);
    }

    public function pay()
    {
        $menu = $this->menu;
        $menufooter = $this->menufooter;
        $categoryes = $this->categoryes;
		$title = 'Оформление заказа';
		$description = 'Оформление заказа';
//        if(!Session::get('cart'))
//            return redirect()->route('index');

        return view('cart.pay', compact('menu', 'categoryes', 'menufooter', 'title', 'description'));
    }

    public function addOrder(Request $request)
    {
        if(!\Auth::check()){
            return 2;
        }

        $products = json_decode($request->products);

        $total = 0;
        $orders = '';

        foreach ($products as $product) {
           $total += $product->price*$product->amount;
           $orders = $orders.$product->name.' - '.$product->amount.' штук<br>';
        }
        
        $order = new Order;
        $order->products=$orders;
        $order->total_sum = $total;
        $order->user_id = \Auth::user()->id;
        $order->address = \Auth::user()->address_one;
        $order->save();

        Session::forget("cart");
        Session::forget("cartcount");
//        return back()
//            ->withInput($request->only('email'))
//            ->withErrors(['email' => trans($response)]);
        return 1;
    }
}
