<?php

namespace App\Http\Controllers\Voyager;

use App\FilterTovar;
use App\Tovary;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\ImageManager;
use TCG\Voyager\Facades\Voyager;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;

class TovariesController extends Controller
{
    protected $redirectTo = '/admin/tovaries';

//    public function edit(int $id): View
//    {
//        $dataTypeContent = Tovary::find($id);
//        $dataType = Voyager::model('DataType')->where('slug', '=', 'tovaries')->first();
//        $isModelTranslatable = is_bread_translatable($dataTypeContent);
//
//        return Voyager::view('voyager::tovaries.edit-add', compact('dataTypeContent', 'dataType', 'isModelTranslatable'));
//    }

    public function update(Request $request, int $id): RedirectResponse
    {
        $product = Tovary::find($id);

        $filename = $product->img;
        $images = $product->images;

        if(request()->img1) {
            $filename = time() . request()->img1->getClientOriginalName() . '.' . request()->img1->getClientOriginalExtension();
            $filename = "tovaries\\$id\\".$filename;
            request()->img1->move(storage_path('app\public\tovaries\\' . $id), $filename);

            $manager = new ImageManager();

            $manager->make(storage_path('app\public\tovaries\\' . $id . '\\' . $filename))->insert('watermark.png')->save(storage_path('app\public\tovaries\\' . $id . '\\' . $filename));
        }

        if($request->images1) {
            if($images){
                $images = substr($images, '1');
                $images = substr($images, '0', '-1');
                $images = "[".$images.",";
            }else
                $images = "[";
            foreach ($request->images1 as $v) {
                $filename_array = time() . $v->getClientOriginalName() . '.' . $v->getClientOriginalExtension();
                $images .= '"tovaries\\\\'.$id.'\\\\' . $filename_array . '",';
                $v->move(storage_path('app\public\tovaries\\' . $id), $filename_array);

                $manager = new ImageManager();

                $manager->make(storage_path('app\public\tovaries\\' . $id . '\\' . $filename_array))->insert('watermark.png')->save(storage_path('app\public\tovaries\\' . $id . '\\' . $filename_array));
            }
            $images = substr($images, '0', '-1');
            $images .= ']';
        }
//        $img->resize(320, 240);

        $relations = '[';
        foreach ($request->relationss as $v)
            $relations .= '"'.$v.'",';

        $relations = substr($relations, '0', '-1');
        $relations .= ']';

        FilterTovar::where('tovary_id', $id)->delete();
        if($request->tovary_belongstomany_filter_category_relationship)
            foreach ($request->tovary_belongstomany_filter_category_relationship as $v){
                $filter = new FilterTovar();

                $filter->filter_category_id = $v;
                $filter->tovary_id = $id;
                $filter->save();
            }

        Tovary::find($id)->update(array_merge([
            'relations' => $relations,
            'img'       => $filename,
            'images'    => $images
        ], $request->all()));

        \Artisan::call('cache:clear');

        return redirect()->to(url($this->redirectTo));
    }

    protected function uploadFiles(): array
    {
        $files = [];

        foreach (request()->all() as $key => $value) {
            if (substr($key, 0, 3) == 'img' && ! empty($value)) {
                $files[] = $value;
            }
        }

        return $files;
    }
}
