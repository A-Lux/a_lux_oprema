<?php

namespace App\Http\Controllers\Voyager;

use App\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;

class NewsController extends Controller
{
    protected $redirectTo = '/admin/news';

    public function update(Request $request, int $id): RedirectResponse
    {
        if(!$request->ajax()) {
            $model = News::find($id);

            if ($request->status == 'on')
                $model->status = 1;
            else
                $model->status = 0;

            $model->update(array_merge($request->all(), [
                'status' => $model->status,
                'url' => $this->GetUrl($request->name),
				'title' => $request->name
            ]));

            \Artisan::call('cache:clear');

            return redirect()->to(url($this->redirectTo));
        }
    }

    public function store(Request $request): RedirectResponse
    {
        if(!$request->ajax())
        {
            $model = new News;

            if($request->status == 'on')
                $model->status = 1;
            else
                $model->status = 0;

            $model->url = $this->GetUrl($request->name);

            $model->create(array_merge($request->all(),[
                'status' => $model->status,
                'url' => $model->url,
				'title' => $request->name
            ]));

            \Artisan::call('cache:clear');

            return redirect()->to(url($this->redirectTo));
        }
    }

    public function GetUrl($url)
    {
        $rus = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я', ' ');
        $lat = array('a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya', '-');
        return str_replace($rus, $lat, $url);
    }
}