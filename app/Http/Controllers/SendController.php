<?php

namespace App\Http\Controllers;

use App\Send;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class SendController extends FrontendController
{
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required'],
        ]);
    }

    public function index(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails())
        {
            $messages = $validator->messages();
            $errors = '';
            $messages = (array)$messages;
            $i = 1;
            foreach($messages as $k => $v1){
                if($i == 1)
                    foreach ($v1 as $v)
                        $errors .= $v[0].'<br/>';
                $i++;
            }
            $array = ['errors' => $errors];

            return json_encode($array);
        }

        $send = new Send();

        $send->name   = $request->name;
        $send->phone  = $request->phone;
        $type = ['type' => $request->type];

        Mail::send('email.index', ['name' => $request->name, 'phone' => $send->phone, 'type' => $request->type], function ($message) use ($type) {
            $message->from('it_yvm@mail.ru');
            $message->to('it_yvm@mail.ru');
            // $message->subject($type['type']);
           $message->subject('Сообщение "Перезвоните мне" с сайта http://oprema.kz');
        });

        if($send->save())
            return 1;
        else{
            return false;
        }
    }
}
