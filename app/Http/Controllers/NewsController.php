<?php

namespace App\Http\Controllers;

use App\News;
use App\Send;
use Illuminate\Http\Request;

class NewsController extends FrontendController
{
    public function one($id)
    {
        $menu = $this->menu;
        $menufooter = $this->menufooter;
        $categories = $this->categoryes;
        $news = News::find($id);
		$title = $news->title;
		$description = $news->description;
		$header_cats = $this->getCategoryes();
        return view('news_one', compact('news', 'menu', 'categories', 'menufooter', 'title', 'description', 'header_cats'));
    }

    public function index()
    {
        $menu = $this->menu;
        $menufooter = $this->menufooter;
        $categories = $this->categoryes;
        $news = News::get();
        foreach ($news as $one) {
            $one->short_description = $this->cutStr($one->short_description, 250);
        }
        $title = 'Новости';
        $header_cats = $this->getCategoryes();
        return view('news', compact('news', 'menu', 'categories', 'menufooter', 'title', 'header_cats'));
    }
}
