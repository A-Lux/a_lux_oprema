<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

class ForgotPasswordController extends \App\Http\Controllers\FrontendController
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $menu = $this->menu;
        // $menufooter = $this->menufooter;
        // $categoryes = $this->categoryes;
		// $title = 'Восстановление пароля';
		// $description = 'Восстановление пароля';
        // $header_cats = $this->getCategoryes();
        $this->middleware('guest');
        $this->showLinkRequestForm();
    }
}
