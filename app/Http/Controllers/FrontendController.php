<?php

namespace App\Http\Controllers;

use App\CategoryTovary;
use App\MenuSite;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public $menu;
    public $menufooter;
    public $categoryes;

    public function __construct()
    {
        $this->menu = $this->getMenu();
        $this->menufooter = $this->getMenufooter();
        $this->categoryes = $this->getCategoryes();
    }

    public function getMenu(){
        return MenuSite::where('status', 1)
            ->where(function($q) {
                $q->where('top_footer',0)
                    ->orWhere('top_footer', 2);
            })
            ->orderBy('sort', 'ASC')
            ->get();
    }

    public function getMenufooter(){
        return MenuSite::where('status', 1)
            ->where(function($q) {
                $q->where('top_footer',1)
                    ->orWhere('top_footer', 2);
            })
            ->orderBy('sort', 'ASC')
            ->get();
    }

    public function getCategoryes(){
        return CategoryTovary::where('parent_id', 0)->get();
    }

    public static function cutStr($str, $length=50, $postfix='...')
    {
        if ( strlen($str) <= $length)
            return $str;

        $temp = substr($str, 0, $length);
        return substr($temp, 0, strrpos($temp, ' ') ) . $postfix;
    }
}
