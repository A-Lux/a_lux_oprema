<?php

namespace App\Http\Controllers;

use App\CategoryTovary;
use App\MenuSite;
use App\User;
use App\Order;
use App\Tovary;
use Illuminate\Http\Request;

class CabinetController extends FrontendController
{
    public function index()
    {
        if(!\Auth::user())
            return redirect('/');

        $menu = $this->menu;
        $menufooter = $this->menufooter;
        $categoryes = $this->categoryes;
		$title = 'Личный кабинет';
		$description = 'Личный кабинет';
        $header_cats = $this->getCategoryes();
        $orders = Order::where('user_id', \Auth::user()->id)->orderBy('id', 'desc')->get();
        return view('cabinet', compact('menu', 'orders', 'categoryes', 'menufooter', 'title', 'description', 'header_cats'));
    }

    public function ld()
    {
        if(!\Auth::user())
            return redirect()->home();

        $menu = $this->menu;
        $menufooter = $this->menufooter;
        $categoryes = $this->categoryes;
		$title = 'Личный кабинет';
		$description = 'Личный кабинет';
		
        return view('cabinet.ld', compact('menu', 'categoryes', 'menufooter', 'title', 'description'));
    }

    public function edit(Request $request)
    {
        $profile = User::where(['id' => \Auth::user()->id])->first();

        $profile->name   = $request->name;
        $profile->phone = $request->phone;
        if(isset($request->password) && $request->password != ''){
            $profile->password   = bcrypt($request->password);
        }
        $profile->phone = $request->phone;
        $profile->address_one = $request->address_one;
        if(isset($request->adress_two)){
            $profile->address_two = $request->address_two;
        }
        $profile->client_index = $request->index;
        $profile->save();

        return redirect()->back();
    }
}
