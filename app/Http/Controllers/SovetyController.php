<?php

namespace App\Http\Controllers;

use App\News;
use App\Send;
use App\Sovety;
use Illuminate\Http\Request;

class SovetyController extends FrontendController
{
    public function index()
    {
        $menu = $this->menu;
        $menufooter = $this->menufooter;
        $categoryes = $this->categoryes;
        $sovety = Sovety::paginate(6);
        return view('sovety.index', compact('sovety', 'menu', 'categoryes', 'menufooter'));
    }

    public function sovet($url)
    {
        $menu = $this->menu;
        $menufooter = $this->menufooter;
        $categoryes = $this->categoryes;
        $sovet = Sovety::where(['url' => $url])->first();
        return view('sovety.sovet', compact('sovet', 'menu', 'categoryes', 'menufooter'));
    }
}
