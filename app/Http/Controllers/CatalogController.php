<?php

namespace App\Http\Controllers;

use App\CategoryTovary;
use App\MenuSite;
use App\Tovary;
use Illuminate\Http\Request;

class CatalogController extends FrontendController
{
    public function catalog($id = 0)
    {
        $category = new \StdClass;
        $category->title = 'Каталог';
        $parents = [];
        if ($id != 0) {
            $category = CategoryTovary::find($id);
            $temp = $category;
            $parents[] = $temp;
            while ($temp->parent_id != 0) {
                $temp_cat = CategoryTovary::find($temp->parent_id);
                $parents[] = $temp_cat;
                $temp = $temp_cat;
            }            
        }
        
        $categories = CategoryTovary::with('allChildren')->where('parent_id', $id)->get();
        $arr = [];
        $arr1 = [];  
        $arr1[]=$id;
        foreach ($categories as $value) {
           $arr1[] = $value->parseTree($arr);
        }         
        $ids = ((collect($arr1)->flatten())->unique())->toArray();
        $goods = Tovary::whereIn('category_id', $ids)->paginate(6);
		$title = $category->title;
        $header_cats = $this->getCategoryes();
        // dd($parents);
        return view('catalog', compact('category', 'categories', 'goods', 'title', 'parents', 'header_cats' ));
    }

    public function product($id)
    {
        $product = Tovary::find($id);
		$title = $product->name;
		$description = $product->description;
        $header_cats = $this->getCategoryes();
        return view('product', compact('product', 'title', 'description', 'header_cats'));
    }


}
