<?php 

namespace App\Http\Filter;

use App\Experience;
use App\Http\Filter\Filter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class ProductFilter extends Filter {

	public $filters = ['query', 'price', 'brand', 'mega', 'category', 'subcategory'];


    public function query($builder, $value) {
        return $builder
            ->where('products.name', 'LIKE', '%'.$value.'%')
            ->orWhere('brand', 'LIKE', '%'.$value.'%');
    }
    
}
