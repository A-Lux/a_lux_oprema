<?php

namespace App\Http\Filter;

use Illuminate\Http\Request;

abstract class Filter {

	protected $request;

	public function __construct(Request $request)
	{
		$this->request = $request;
	}

	public function apply($builder)
	{
		foreach ($this->filters() as $filter => $value) {
			if (method_exists($this, $filter) && $value != null) {
				$builder = $this->$filter($builder, $value);
			}
		}

		return $builder;
	}

	public function filters()
	{
		$filters = $this->request->only($this->filters);
		if(isset($this->request->mega)) {
			$filters['mega'] = $this->request->mega;
		}
		if(isset($this->request->category)) {
			$filters['category'] = $this->request->category;
		}
		if(isset($this->request->subcategory)) {
			$filters['subcategory'] = $this->request->subcategory;
		}
		return array_filter($filters);
	}

}
