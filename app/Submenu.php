<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submenu extends Model
{
    public static $req_menu;
    public static $inc = 0;

    public static function getmenu($id, $p_id)
    {
        $menu = Submenu::all();
        $arr_cat = [];
        foreach($menu as $v){
            if(empty($arr_cat[$v['parent_id']])) {
                $arr_cat[$v['parent_id']] = [];
            }
            $arr_cat[$v['parent_id']][] = $v;
        }
        self::getTree($arr_cat, $id, $p_id);
    }

//    public static function getmenutovar($id, $p_id)
//    {
//        $menu = Submenu::all();
//        $arr_cat = [];
//        foreach($menu as $v){
//            if(empty($arr_cat[$v['parent_id']])) {
//                $arr_cat[$v['parent_id']] = [];
//            }
//            $arr_cat[$v['parent_id']][] = $v;
//        }
//        self::getTreetovar($arr_cat, $id, $p_id);
//    }

    public static function getmenufooter()
    {
        echo '<div class="col-lg-3 col-md-4 col-sm-6">
                             <div class="fot-list">
                                <ul>';
        $menu = Submenu::all();
        $arr_cat = [];
        $count = 0;
        foreach($menu as $v){
            if($v['parent_id'] != 0)
                $count++;
            if(empty($arr_cat[$v['parent_id']])) {
                $arr_cat[$v['parent_id']] = [];
            }
            $arr_cat[$v['parent_id']][] = $v;
        }
        self::getTreeForMenuFooter($arr_cat, $count = $count / 4 + 1);
        echo '    </ul>
              </div>
          </div>';
    }

    protected static function getTreeForMenuFooter($arr, $count, $parent_id = 0, $newarray = [], $parent_inc = 0, $inc = 0)
    {
        if(empty($arr[$parent_id])) {
            return 0;
        }
        for($i = 0; $i < count($arr[$parent_id]);$i++){
            if(!isset($newarray['parent'][$arr[$parent_id][$i]['parent_id']])){
                $newarray['parent'][$arr[$parent_id][$i]['id']] = 1;
                $parent_inc = $i;
            }
            else{
                $parent_url = $arr[0][$parent_inc]['url'];
                echo '<li><a href="/category/'.$parent_url.'/'.$arr[$parent_id][$i]['url'].'">'.$arr[$parent_id][$i]['name'].'</a></li>';

                self::$inc++;
                if(self::$inc % $count == 0)
                    echo '          </ul>
					 		</div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6">
                             <div class="fot-list">
                                <ul>';

            }

            self::getTreeForMenuFooter($arr, $count, $arr[$parent_id][$i]['id'], $newarray, $parent_inc, $inc);
        }
    }


    protected static function getTree($arr, $id = 0, $p_id = 0, $parent_id = 0, $parent = 0, $inc = 0)
    {
        if(empty($arr[$parent_id])) {
            return 0;
        }

        for($i = 0; $i < count($arr[$parent_id]);$i++) {

            if($arr[$parent_id][$i]['parent_id'] == 0){
                $disabled = "";
                $name = "";
                $inc = $i;
            }else{
                $disabled = "disabled";
                $name = "-- ";
            }

            $selected = "";
            if($arr[$parent_id][$i]['id'] == $p_id)
                $selected = "selected";

            echo '<option '.$disabled.' '.$selected.' value="'.$arr[$parent_id][$i]['id'].'">'.$name.$arr[$parent_id][$i]['name'].'</option>';

            self::getTree($arr, $id, $p_id, $arr[$parent_id][$i]['id'], $arr[$parent_id][$i]['parent_id'], $inc);
        }
        return 1;
    }

    protected static function getTreeCatalog($arr, $id = 0, $p_id = 0, $parent_id = 0, $inc = 0)
    {
        if(empty($arr[$parent_id])) {
            return 0;
        }
        for($i = 0; $i < count($arr[$parent_id]);$i++) {
            $name = "";

            $disabled = "";
            if($inc==2 || $id == $arr[$parent_id][$i]['id'])
                $disabled = "disabled";

            if($inc == 2){
                $name = "---- ";
            }
            if($inc == 1){
                $name = "-- ";
                $inc++;
            }
            if($arr[$parent_id][$i]['parent_id'] == 0)
                $name = "";

            $selected = "";
            if($arr[$parent_id][$i]['id'] == $p_id)
                $selected = "selected";

            echo '<option '.$disabled.' '.$selected.' value="'.$arr[$parent_id][$i]['id'].'">'.$name.$arr[$parent_id][$i]['name'].'</option>';

            if($arr[$parent_id][$i]['parent_id'] == 0)
                $inc = 1;
            self::getTree($arr, $id, $p_id, $arr[$parent_id][$i]['id'], $inc);
        }
        return 1;
    }

//    protected static function getTreetovar($arr, $id = 0, $p_id = 0, $parent_id = 0, $parent = 0, $inc = 0)
//    {
//        if(empty($arr[$parent_id])) {
//            return 0;
//        }
//
//        for($i = 0; $i < count($arr[$parent_id]);$i++) {
//            $name = "";
//
//            $disabled = "disabled";
//
//            if(isset($arr[$parent][$inc]['parent_id']) && $arr[$parent][$inc]['parent_id'] == 0){
//                $name = "-- ";
//            }else{
//                $disabled = "";
//                $name = "---- ";
//            }
//            if($arr[$parent_id][$i]['parent_id'] == 0){
//                $name = "";
//                $inc = $i;
//            }
//
//            $selected = "";
//            if($arr[$parent_id][$i]['id'] == $p_id)
//                $selected = "selected";
//
//            echo '<option '.$disabled.' '.$selected.' value="'.$arr[$parent_id][$i]['id'].'">'.$name.$arr[$parent_id][$i]['name'].'</option>';
//
//            self::getTreetovar($arr, $id, $p_id, $arr[$parent_id][$i]['id'], $arr[$parent_id][$i]['parent_id'], $inc);
//        }
//        return 1;
//    }
    public static function getmenutovar($id, $p_id)
    {
        $menu = Submenu::all();
        $arr_cat = [];
        foreach($menu as $v){
            if(empty($arr_cat[$v['parent_id']])) {
                $arr_cat[$v['parent_id']] = [];
            }
            $arr_cat[$v['parent_id']][] = $v;
        }
        self::getTreetovar($arr_cat, $id, $p_id);
    }

    protected static function getTreetovar($arr, $id = 0, $p_id = 0, $parent_id = 0, $parent = 0, $inc = 0)
    {
        if(empty($arr[$parent_id])) {
            return 0;
        }

        for($i = 0; $i < count($arr[$parent_id]);$i++) {

            if($arr[$parent_id][$i]['parent_id'] == 0){
                $disabled = "disabled";
                $name = "";
                $inc = $i;
            }else{
                $disabled = "";
                $name = "-- ";
            }

            $selected = "";
            if($arr[$parent_id][$i]['id'] == $p_id)
                $selected = "selected";

            echo '<option '.$disabled.' '.$selected.' value="'.$arr[$parent_id][$i]['id'].'">'.$name.$arr[$parent_id][$i]['name'].'</option>';

            self::getTreetovar($arr, $id, $p_id, $arr[$parent_id][$i]['id'], $arr[$parent_id][$i]['parent_id'], $inc);
        }
        return 1;
    }
    //////////////////////////////
    public function tovary()
    {
        return $this->hasMany(Tovary::class, 'category_id');
    }

    public function child()
    {
        return $this->hasMany(Submenu::class, 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(Submenu::class, 'parent_id');
    }
}
