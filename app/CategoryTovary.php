<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryTovary extends Model
{
    public static $req_menu;
	
    public static function getmenu($id, $p_id)
    {
		$menu = CategoryTovary::all();
		$arr_cat = [];
		foreach($menu as $v){
			if(empty($arr_cat[$v['parent_id']])) {
			    $arr_cat[$v['parent_id']] = [];
			}
			$arr_cat[$v['parent_id']][] = $v;
		}
		self::getTree($arr_cat, $id, $p_id);
    }

    public static function getmenutovar($id, $p_id)
    {
        $menu = CategoryTovary::all();
        $arr_cat = [];
        foreach($menu as $v){
            if(empty($arr_cat[$v['parent_id']])) {
                $arr_cat[$v['parent_id']] = [];
            }
            $arr_cat[$v['parent_id']][] = $v;
        }
        self::getTreetovar($arr_cat, $id, $p_id);
    }

    public static function getmenuforcatalog($id)
    {
		$menu = CategoryTovary::all();
		$arr_cat = [];
		foreach($menu as $v){
			if(empty($arr_cat[$v['parent_id']])) {
			    $arr_cat[$v['parent_id']] = [];
			}
			$arr_cat[$v['parent_id']][] = $v;
		}
		self::getTreeForCatalog($arr_cat, $id);
    }
		
    protected static function getTreeForCatalog($arr, $parent_id, $newarray = [])
    {
		 if(empty($arr[$parent_id])) {
		     return 0;
		 }
		 for($i = 0; $i < count($arr[$parent_id]);$i++){
		     if(!isset($newarray['parent'][$arr[$parent_id][$i]['parent_id']])){
                 $newarray['parent'][$arr[$parent_id][$i]['id']] = 1;
		         if($i == 0)
		         echo '<div class="collapse-text">
                            <a ><p><img src="images/collapse-arrow.png" alt="">'.$arr[$parent_id][$i]['name'].'</p></a>
                            <div class="collapse-list">
                                <ul>';
		         else
                     echo '     </ul>
                            </div>
                        <div class="collapse-text">
                            <a><p><img src="images/collapse-arrow.png" alt="">'.$arr[$parent_id][$i]['name'].'</p></a>
                            <div class="collapse-list">
                                <ul>';
             }
		     else{
		         $url = explode('/', $_SERVER['REQUEST_URI']);
                echo '<li><a href="/'.$url[1].'/'.$url[2].'/'.$arr[$parent_id][$i]['url'].'">'.$arr[$parent_id][$i]['name'].'</a></li>';
             }

             self::getTreeForCatalog($arr, $arr[$parent_id][$i]['id'], $newarray);

		 }
        echo '      </ul>
            </div>';
    }


    protected static function getTree($arr, $id = 0, $p_id = 0, $parent_id = 0, $parent = 0, $inc = 0)
    {
        if(empty($arr[$parent_id])) {
            return 0;
        }

        for($i = 0; $i < count($arr[$parent_id]);$i++) {
            $name = "";

            $disabled = "";

            if(isset($arr[$parent][$inc]['parent_id']) && $arr[$parent][$inc]['parent_id'] == 0){
                $name = "-- ";
            }else{
                $disabled = "disabled";
                $name = "---- ";
            }
            if($arr[$parent_id][$i]['parent_id'] == 0){
                $name = "";
                $inc = $i;
            }

            $selected = "";
            if($arr[$parent_id][$i]['id'] == $p_id)
                $selected = "selected";

            echo '<option '.$disabled.' '.$selected.' value="'.$arr[$parent_id][$i]['id'].'">'.$name.$arr[$parent_id][$i]['name'].'</option>';

            self::getTree($arr, $id, $p_id, $arr[$parent_id][$i]['id'], $arr[$parent_id][$i]['parent_id'], $inc);
        }
        return 1;
    }

    protected static function getTreeCatalog($arr, $id = 0, $p_id = 0, $parent_id = 0, $inc = 0)
    {
        if(empty($arr[$parent_id])) {
            return 0;
        }
        for($i = 0; $i < count($arr[$parent_id]);$i++) {
            $name = "";

            $disabled = "";
            if($inc==2 || $id == $arr[$parent_id][$i]['id'])
                $disabled = "disabled";

            if($inc == 2){
                $name = "---- ";
            }
            if($inc == 1){
                $name = "-- ";
                $inc++;
            }
            if($arr[$parent_id][$i]['parent_id'] == 0)
                $name = "";

            $selected = "";
            if($arr[$parent_id][$i]['id'] == $p_id)
                $selected = "selected";

            echo '<option '.$disabled.' '.$selected.' value="'.$arr[$parent_id][$i]['id'].'">'.$name.$arr[$parent_id][$i]['name'].'</option>';

            if($arr[$parent_id][$i]['parent_id'] == 0)
                $inc = 1;
            self::getTree($arr, $id, $p_id, $arr[$parent_id][$i]['id'], $inc);
        }
        return 1;
    }

    protected static function getTreetovar($arr, $id = 0, $p_id = 0, $parent_id = 0, $parent = 0, $inc = 0)
    {
        if(empty($arr[$parent_id])) {
            return 0;
        }

        for($i = 0; $i < count($arr[$parent_id]);$i++) {
            $name = "";

            $disabled = "disabled";

            if(isset($arr[$parent][$inc]['parent_id']) && $arr[$parent][$inc]['parent_id'] == 0){
                $name = "-- ";
            }else{
                $disabled = "";
                $name = "---- ";
            }
            if($arr[$parent_id][$i]['parent_id'] == 0){
                $name = "";
                $inc = $i;
            }

            $selected = "";
            if($arr[$parent_id][$i]['id'] == $p_id)
                $selected = "selected";

            echo '<option '.$disabled.' '.$selected.' value="'.$arr[$parent_id][$i]['id'].'">'.$name.$arr[$parent_id][$i]['name'].'</option>';

            self::getTreetovar($arr, $id, $p_id, $arr[$parent_id][$i]['id'], $arr[$parent_id][$i]['parent_id'], $inc);
        }
        return 1;
    }
	//////////////////////////////
    public function tovary()
    {
        return $this->hasMany(Tovary::class, 'category_id');
    }

    public function child()
    {
        return $this->hasMany(CategoryTovary::class, 'parent_id');
    }

    public function allChildren()
    {
        return $this->child()->with('allChildren');
    }

    public function parseTree($arr){
        $arr[] = $this->id;
        if($this->allChildren->count() > 0) {
            foreach ($this->allChildren as $category) {
                $arr[] = $category->id;
                return $category->parseTree($arr);
            }   
        }else {
            return $arr;
        }
    }
}
