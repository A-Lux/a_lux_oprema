<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tovary extends Model
{
    public function category()
    {
        return $this->belongsTo(CategoryTovary::class, 'category_id');
    }
}
