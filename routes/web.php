<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'SiteController@index')->name('index');

Route::get('/news/', 'NewsController@index')->name('news');

Route::get('/news/{id}', 'NewsController@one')->name('news_one');

Route::get('/contacts', 'SiteController@contacts')->name('contacts');

Route::get('/delivery', 'SiteController@delivery')->name('delivery');

Route::get('/about', 'SiteController@about')->name('about');

Route::get('/partners', 'SiteController@partners')->name('partners');

Route::get('/services', 'SiteController@services')->name('services');

Route::get('/category/{id?}', 'CatalogController@catalog')->name('category');

Route::get('/product/{id}', 'CatalogController@product')->name('product');

Route::get('/cabinet', 'CabinetController@index')->name('cabinet');

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');;

Route::get('/cart', 'CartController@order')->name('cart');

Route::get('/add', 'CartController@addToCart')->name('add');

Route::get('/update', 'CartController@updateCart')->name('update');

Route::get('/remove', 'CartController@delete')->name('remove');

Route::post('/cabinet', 'CabinetController@edit')->name('user_edit');

Route::post('/request', 'SendController@index')->name('request');

Route::post('/edit', 'CabinetController@edit')->name('edit');

Route::post('/order', 'CartController@addOrder')->name('order');

Route::get('/search', 'SearchController@index')->name('search');
